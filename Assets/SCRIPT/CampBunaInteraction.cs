﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class CampBunaInteraction : MonoBehaviour
{

	int nextText;
	int nextDecision=0;
	bool firstConversation;
	public static bool options;

	float dialogueXSlideDistance=3f;
	float xSD;
	float dialogueYOffSet = 7f;

	int[] decsionsNums = new int[] { 5};



	Dialogue first = new Dialogue(
		"first",
		new List<string> {
			"Is that...a ring? Hand me it now!",
			"No!",
			"I will give you extra rations and spare you from a day of labor.",
			"No! It's my family heirloom!",
			"Give it or else you will receive a severe whipping!",

			"[1]: Hand him the ring\n[2]: Keep the ring\n[3]: Hope for God's protection" , 

			"Ok. Here. What about my extra rati...?" ,
			"You're not getting anything! Next time you disobey you will be going straight to the furnace!" ,

			"No! I will never give the ring up!",
			"Then I'll just take it from you!!",
			"Let me go!",
			"You're coming with me!",

			"No! God will protect me!",
			"Let's see if God will protect you from this whip!!",
			"No! Please don't! I'll give it to you!! Just don't whip me please!!",
			"Too late for that. I already gave you a choice. Where is your God now?",
		},
		new int[] { 5 ,2,4 , 4}
	);

	// Use this for initialization
	void Start()
	{
		nextText = 0;
		firstConversation = false;
		options = false;
		xSD = dialogueXSlideDistance;
	}

	// Update is called once per frame
	void Update()
	{

		if (firstConversation && !options)
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				//Debug.Log (nextText.ToString() + " : "  + xSD.ToString());

				if (nextText == decsionsNums [nextDecision]) {
					options = true;
					first.comingDecision = true;
					first.Play(this.transform.position.x - xSD, this.transform.position.y + dialogueYOffSet);
				}

				else 
				{
					first.Play(this.transform.position.x - xSD, this.transform.position.y + dialogueYOffSet);
					if(xSD > 0)
					{
						xSD = 0;
					}
					else
					{
						xSD = dialogueXSlideDistance;
					}
				}
				nextText++;

			}
		}
		if (options)
		{
			if (Input.GetKeyDown(KeyCode.Alpha1)|| Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3))
			{
				//Debug.Log ("HELLO");
				if (first.Decision (this.transform.position.x - xSD, this.transform.position.y + dialogueYOffSet, Input.inputString, false))
				{
					
					if(xSD > 0)
					{
						xSD = 0;
					}
					else
					{
						xSD = dialogueXSlideDistance;
					}
					options = false;
				}


			}            
		}
	}
	public void OnTriggerEnter(Collider c)
	{
		if (c.gameObject.name.Equals("Player") && !firstConversation)
		{           
			first.Play(this.transform.position.x, this.transform.position.y + dialogueYOffSet);
			firstConversation = true;
			nextText++;

		}
	}
}
