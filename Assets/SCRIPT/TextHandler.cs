﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class TextHandler : MonoBehaviour {

	public static Canvas canvas;
	public static GameObject speakBox;
	public Canvas thisCanvas;
	public GameObject thisSpeakBox;
	static GameObject speakboxInstance;
	static bool cameraPan = false;
	static float cameraPanx;
	static float cameraPany;
	static float cameraPansize;
	public static bool convo;
	public Camera cam;



	// Use this for initialization
	void Start () {
		canvas = thisCanvas;
		speakBox = thisSpeakBox;
		convo = false;
	}

	void Update()
	{
		if (cameraPan) {
			cam.transform.position = Vector3.Lerp (cam.transform.position, new Vector3 (cameraPanx, cameraPany, -50), 0.05f);
			cam.orthographicSize = cam.orthographicSize - ((cam.orthographicSize - cameraPansize) * 0.05f);
		}

	}

	public static void createText(float x , float y , string text)
	{

		Vector3 temp = new Vector3 (x, y, -10);
		GameObject.Destroy (speakboxInstance);
		speakboxInstance = (GameObject)Instantiate (speakBox, temp, Quaternion.identity);
		speakboxInstance.transform.SetParent (canvas.transform, false);
		speakboxInstance.GetComponentInChildren<Text> ().text = text;
		cameraPan = true;
		cameraPanx = x;
		cameraPany = y;
		cameraPansize = 10;
		convo = true;


		//Camera.current.transform.position = new Vector3 (x, y, -50);
		//Camera.current.orthographicSize = 10;

	}
	public static void deleteText()
	{		
		GameObject.Destroy (speakboxInstance);
		cameraPanx = 0;
		cameraPany = 3;
		cameraPansize = 15;
		convo = false;
	}

}
