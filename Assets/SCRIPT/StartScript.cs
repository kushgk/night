﻿using UnityEngine;
using System.Collections;

public class StartScript : MonoBehaviour {
	GameObject wakeUpFadeBlackScreen;
	// Use this for initialization
	void Start () {
		wakeUpFadeBlackScreen = GameObject.Find("wake");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			StartCoroutine(WaitAndLoad());
		}
	}
	IEnumerator WaitAndLoad()
	{
		wakeUpFadeBlackScreen.GetComponent<Animator>().SetBool("fade", true);
		yield return new WaitForSeconds(2.5f);
		SceneManagement.loadLevel();
	}
}
