﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class BuchenwaldInteraction : MonoBehaviour
{

	int nextText;
	int nextDecision = 0;
	bool firstConversation;
	public static bool options;

	float dialogueXSlideDistance = 3f;
	float xSD;
	float dialogueYOffSet = 7f;

	int[] decsionsNums = new int[] {5 };



	Dialogue first = new Dialogue(
		"first",
		new List<string> {
			"Hey! Do you want to leave this place?",
			"Huh? Of course I want to!",
			"We can get you out...",
			"What? What are you guys planning??",
			"Are you willing to join...?",

			"[1]: Sell them out for bread\n[2]: Nod and shake his hand\n[3]: Pray for help" ,

			"Yeah...sure I'll join. Let me go and get my stuff...",
			"Ok, but be quick!",
		
			"Alright...count me in.",
			"Let's get out here together!",

			"Oh please God what should I do?",
			"Stop praying kid. It won't do you any good.",
		},
		new int[] {5, 2, 2, 2}
	);

	// Use this for initialization
	void Start()
	{
		nextText = 0;
		firstConversation = false;
		options = false;
		xSD = dialogueXSlideDistance;
	}

	// Update is called once per frame
	void Update()
	{

		if (firstConversation && !options)
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				//Debug.Log (nextText.ToString() + " : "  + xSD.ToString());

				if (nextText == decsionsNums[nextDecision]){
					options = true;
					first.comingDecision = true;
					first.Play(this.transform.position.x - xSD, this.transform.position.y + dialogueYOffSet);
				}

				else
				{
					first.Play(this.transform.position.x - xSD, this.transform.position.y + dialogueYOffSet);
					if (xSD > 0)
					{
						xSD = 0;
					}
					else
					{
						xSD = dialogueXSlideDistance;
					}
				}
				nextText++;

			}
		}
		if (options)
		{
			if (Input.GetKeyDown("1") || Input.GetKeyDown("2") || Input.GetKeyDown("3"))
			{
				//Debug.Log ("HELLO");
				if (first.Decision(this.transform.position.x - xSD, this.transform.position.y + dialogueYOffSet, Input.inputString, false))
				{

					if (xSD > 0)
					{
						xSD = 0;
					}
					else
					{
						xSD = dialogueXSlideDistance;
					}
					options = false;
				}


			}
		}
	}
	public void OnTriggerEnter(Collider c)
	{
		if (c.gameObject.name.Equals("Player") && !firstConversation)
		{
			first.Play(this.transform.position.x, this.transform.position.y + dialogueYOffSet);
			firstConversation = true;
			nextText++;

		}
	}
}
