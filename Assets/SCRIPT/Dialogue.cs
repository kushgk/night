﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
[System.Serializable]
public class Dialogue : MonoBehaviour {
	private string name;
	private List<string> texts;
	private int[] decision;


	private int speechNum=0;
	private int optionNum = 0;
	int branchLeft;

	public bool comingDecision;




	private bool decisionMade;
	private string decisionDamageType;

    void Start()
    {
        comingDecision = false;
        decisionMade = false;
    }
	public Dialogue(string n, List<string> t,int[] r)
	{
		name = n;
		texts = t;
		decision = r;
	}
	public void Play(float x, float y)
	{
		TextHandler.deleteText();
		if(decisionMade)
		{
			TextHandler.createText(x, y, texts[speechNum]);
			speechNum++;
			branchLeft--;		
			if (branchLeft < 1) {
				speechNum = sumlength(4);
				decisionMade = false;
			}

		}
		else if (speechNum < texts.Count)
		{
			if (comingDecision) 
			{
				string[] temp = texts [speechNum].Split ('[');
		
				foreach (string s in temp) {
					Debug.Log (s);
				}
				string finalOptions = "";
				if (StatusUpdate.mentalStatus > 0.5f)
				{
					finalOptions += "[" + temp [1];
				} 
				else
				{
					finalOptions += "<color=red>[" + temp [1] + "</color>";
				}
				//Debug.Log (finalOptions);
				if (StatusUpdate.physicalStatus > 0.5f) 
				{
					finalOptions += "[" + temp [2];
				} 
				else
				{
					finalOptions += "<color=red>[" + temp [2] + "</color>";
				}
				//Debug.Log (finalOptions);
				if (StatusUpdate.faithStatus > 0.5f)
				{
					finalOptions += "[" + temp [3];
				} 
				else 
				{
					finalOptions += "<color=red>[" + temp [3] + "</color>";
				}
				//Debug.Log (finalOptions);
				TextHandler.createText (x, y, finalOptions);
			}
			else
			{
				TextHandler.createText(x, y, texts[speechNum]);
			}



			speechNum++;
		}
		else
		{            
			speechNum = 0;
			optionNum = 0;
			StatusUpdate.Hit(decisionDamageType);
		}

		return;
	}
	public bool Decision(float x, float y, string decisionL, bool damageOrder)
	{

	
		if (decisionL == "1" && StatusUpdate.mentalStatus > 0.5f)
		{
			decisionMade = true;
			branchLeft = decision [optionNum + 1]; 

			speechNum = sumlength(1);
			Play (x, y);
			decisionDamageType = "m";
			return true;

		}
		else if (decisionL == "2" && StatusUpdate.physicalStatus > 0.5f)
		{
			decisionMade = true;
			branchLeft = decision [optionNum + 2]; 
			speechNum = sumlength(2);
			Play (x, y);
			decisionDamageType = "p"; 
			return true;


		}
		else if (decisionL == "3" && StatusUpdate.faithStatus > 0.5f)
		{
			decisionMade = true;
			branchLeft = decision [optionNum + 3]; 
			speechNum = sumlength(3);
			Play (x, y);
			decisionDamageType = "f";
			return true;

		} 
		return false;

		//speechNum = decision [optionNum + 2] + 1;
		//optionNum++;
	}

	public int sumlength(int num)
	{
		int length = 1;
		for (int i = 0; i < num; i++) {
			length += decision [i];
		}

		return length;

	}

}
