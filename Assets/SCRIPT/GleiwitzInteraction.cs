﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class GleiwitzInteraction : MonoBehaviour
{

    int nextText;
    int nextDecision = 0;
    bool firstConversation;
    public static bool options;

    float dialogueXSlideDistance = 3f;
    float xSD;
    float dialogueYOffSet = 7f;

    int[] decsionsNums = new int[] {11 };



    Dialogue first = new Dialogue(
        "first",
        new List<string> {
            "What's the matter?",
            "Why is there such cruelty in this world...I’ve lost so much...Is there anything else to lose?",
            "What do you mean?",
            "How could anyone let something like this happen...?",
            "It is God's will.",
            "God?",
             "Believe in God. We shall embrace the trial given by God himself.",
            "Maybe...but...",
            "Believe in God, respect God, and pray day and night.",            
            "But still...This is too much...",
            "No matter what happens. It's all God's will. You must embrace the trial!",

            "[1]: Encourage both yourself and the man\n[2]: Walk away\n[3]: Believe the man" ,

            "You know what I agree with you! We will overcome this together!" ,
            "Unless the God wants us both dead..." ,

            "I'm not so sure about this...",
            "Do you doubt his will!?",
            "He has not helped us so far...",
            "How dare you!! You must be beaten!",

			"I appreciate your advise. I will continue to pray...",
            "Praying...will do you no good...",
            "But you just said this is God's wi...",
            "I don't know either! Who knows if God exists!? I've had enough already!!!",
        },
        new int[] { 11, 2, 4, 4 }
    );

    // Use this for initialization
    void Start()
    {
        nextText = 0;
        firstConversation = false;
        options = false;
        xSD = dialogueXSlideDistance;
    }

    // Update is called once per frame
    void Update()
    {

        if (firstConversation && !options)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //Debug.Log (nextText.ToString() + " : "  + xSD.ToString());

                if (nextText == decsionsNums[nextDecision]){
                    options = true;
                    first.comingDecision = true;
                    first.Play(this.transform.position.x - xSD, this.transform.position.y + dialogueYOffSet);
                }

                else
                {
                    first.Play(this.transform.position.x - xSD, this.transform.position.y + dialogueYOffSet);
                    if (xSD > 0)
                    {
                        xSD = 0;
                    }
                    else
                    {
                        xSD = dialogueXSlideDistance;
                    }
                }
                nextText++;

            }
        }
        if (options)
        {
            if (Input.GetKeyDown("1") || Input.GetKeyDown("2") || Input.GetKeyDown("3"))
            {
                //Debug.Log ("HELLO");
                if (first.Decision(this.transform.position.x - xSD, this.transform.position.y + dialogueYOffSet, Input.inputString, false))
                {

                    if (xSD > 0)
                    {
                        xSD = 0;
                    }
                    else
                    {
                        xSD = dialogueXSlideDistance;
                    }
                    options = false;
                }


            }
        }
    }
    public void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.name.Equals("Player") && !firstConversation)
        {
            first.Play(this.transform.position.x, this.transform.position.y + dialogueYOffSet);
            firstConversation = true;
            nextText++;

        }
    }
}
