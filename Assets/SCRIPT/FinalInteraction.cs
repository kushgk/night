﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class FinalInteraction : MonoBehaviour
{

	int nextText;
	int nextDecision = 0;
	bool firstConversation;
	public static bool options;

	float dialogueXSlideDistance = 3f;
	float xSD;
	float dialogueYOffSet = 7f;

	int[] decsionsNums = new int[] {7 };

	GameObject wakeUpFadeBlackScreen;

	Dialogue first = new Dialogue(
		"first",
		new List<string> {
			"Don’t move. Stop right there…!! Which one of you is A7713?",
			"Over here.",
			"Alright kid. Are these the folks who were conspiring to escape?",
			"Y...yeah...these are the ones. Now can I have my bread…?",
			"Bread...? Bread huh…? Get down pig!! Who said I’ll give you bread!? I’m hanging you all!!",
			"Wha…!? H...hold on!!! Y...you said…!! N...no…! Please…!! No!!",
			"Shut up pig. To the noose! All of you!"
		},
		new int[] { 7, 0, 0,0 }
	);

	// Use this for initialization
	void Start()
	{
		wakeUpFadeBlackScreen = GameObject.Find("wake");
		nextText = 0;
		firstConversation = false;
		options = false;
		xSD = dialogueXSlideDistance;
	}

	// Update is called once per frame
	void Update()
	{

		if (firstConversation && !options)
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				//Debug.Log (nextText.ToString() + " : "  + xSD.ToString());

				if (nextText == decsionsNums[nextDecision]){
					//options = true;
					first.comingDecision = true;
					first.Play(this.transform.position.x - xSD, this.transform.position.y + dialogueYOffSet);
					StartCoroutine(WaitAndLoad());
				}

				else
				{
					first.Play(this.transform.position.x - xSD, this.transform.position.y + dialogueYOffSet);
					if (xSD > 0)
					{
						xSD = 0;
					}
					else
					{
						xSD = dialogueXSlideDistance;
					}
				}
				nextText++;

			}
		}
		if (options)
		{
			if (Input.GetKeyDown("1") || Input.GetKeyDown("2") || Input.GetKeyDown("3"))
			{
				//Debug.Log ("HELLO");
				if (first.Decision(this.transform.position.x - xSD, this.transform.position.y + dialogueYOffSet, Input.inputString, false))
				{

					if (xSD > 0)
					{
						xSD = 0;
					}
					else
					{
						xSD = dialogueXSlideDistance;
					}
					options = false;
				}


			}
		}
	}
	public void OnTriggerEnter(Collider c)
	{
		if (c.gameObject.name.Equals("Player") && !firstConversation)
		{
			first.Play(this.transform.position.x, this.transform.position.y + dialogueYOffSet);
			firstConversation = true;
			nextText++;

		}
	}


	IEnumerator WaitAndLoad()
	{
		wakeUpFadeBlackScreen.GetComponent<Animator>().SetBool("fade", true);
		yield return new WaitForSeconds(2.5f);
		SceneManagement.loadLevel();
	}


}
