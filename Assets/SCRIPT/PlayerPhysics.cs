﻿using UnityEngine;
using System.Collections;

public class PlayerPhysics : MonoBehaviour {

	bool jump;
	Rigidbody rigid;
	float maxSpeed = 4f;

    public GameObject playerAnim;

	SpriteRenderer r;
	// Use this for initialization
	void Start () {
		jump = false;
		rigid = GetComponent<Rigidbody> ();
		Physics.gravity = new Vector3 (0, -20, 0);

        StatusUpdate.startBool = true;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (!TextHandler.convo) {
			if (Input.GetKey (KeyCode.A)) {
				rigid.AddForce (new Vector3 (-60, 0, 0));
				playerAnim.transform.localEulerAngles = new Vector3(0,180,0);
                playerAnim.GetComponent<Animator>().SetBool("walk", true);

			}
            
			if (Input.GetKey (KeyCode.D)) {
                playerAnim.transform.localEulerAngles = new Vector3(0, 0, 0);

                rigid.AddForce (new Vector3 (60, 0, 0));
                playerAnim.GetComponent<Animator>().SetBool("walk", true);
			}
            
            if (Input.GetKey (KeyCode.W)) {
				if (!jump) {
					rigid.AddForce (new Vector3 (0, 1500, 0));
					jump = true;
				}
			}
			if (rigid.velocity.x > maxSpeed) {
				rigid.velocity = new Vector3 (maxSpeed, rigid.velocity.y, rigid.velocity.z);
			}
			if (rigid.velocity.x < -maxSpeed) {
				rigid.velocity = new Vector3 (-maxSpeed, rigid.velocity.y, rigid.velocity.z);
			}



			if (rigid.velocity.y < 0) {
				rigid.AddForce (new Vector3 (0, -60, 0));

			}
			if (rigid.velocity.y > 1) {
				rigid.AddForce (new Vector3 (0, 10, 0));

			}
		}
        else
        {
			rigid.velocity = new Vector3 (0, 0, 0);
            playerAnim.GetComponent<Animator>().SetBool("walk", false);
        }
        if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
        {
            playerAnim.GetComponent<Animator>().SetBool("walk", false);
        }



    }
	public void OnCollisionEnter(Collision c)
	{
		jump = false;
	}
}
