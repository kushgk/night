﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class AushchwitzInteraction : MonoBehaviour {
	int nextText;
	int nextDecision=0;
	bool firstConversation;
	public static bool options;

	float dialogueXSlideDistance=3f;
	float xSD;
	float dialogueYOffSet = 7f;

	int[] decsionsNums = new int[] {5};



	Dialogue first = new Dialogue(
		"first",
		new List<string> {
			"Please, I need food! I beg of you!",
			"Sorry...I can't spare any rations.",
			"I need just a little bit...I cannot go on with out food...",
			"Sorry.",
			"Please I beg of you...give me your bread...!",


			"[1]: Don't give your rations\n[2]: Give your rations\n[3]: Preach" , 

			"No...ask someone else.",
			"Then I'll just have to take it from you!!",
			"No! Get off me!!",

			"Here take half of my bread." , 
			"Thank you! May God bless you!",

			"Go try and steal some food. God will protect you." , 
			"I've tried that and all I got were beatings..." , 
			"What are you talking about?", 
			"I don't think God is watching over us anymore",
			"What!? Have you gone mad!?",
			"You'll understand eventually..."

		},
		new int[] { 5 , 3,2  , 6}
	);

	// Use this for initialization
	void Start()
	{
		nextText = 0;
		firstConversation = false;
		options = false;
		xSD = dialogueXSlideDistance;
	}

	// Update is called once per frame
	void Update()
	{

		if (firstConversation && !options)
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				//Debug.Log (nextText.ToString() + " : "  + xSD.ToString());

				if (nextText == decsionsNums [nextDecision]) {
					options = true;
					first.comingDecision = true;
					first.Play(this.transform.position.x - xSD, this.transform.position.y + dialogueYOffSet);
				}

				else 
				{
					first.Play(this.transform.position.x - xSD, this.transform.position.y + dialogueYOffSet);
					if(xSD > 0)
					{
						xSD = 0;
					}
					else
					{
						xSD = dialogueXSlideDistance;
					}
				}
				nextText++;

			}
		}
		if (options)
		{
			if (Input.GetKeyDown(KeyCode.Alpha1)|| Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3))
			{
				//Debug.Log ("HELLO");
				if (first.Decision (this.transform.position.x - xSD, this.transform.position.y + dialogueYOffSet, Input.inputString, false))
				{

					if(xSD > 0)
					{
						xSD = 0;
					}
					else
					{
						xSD = dialogueXSlideDistance;
					}
					options = false;
				}


			}            
		}
	}
	public void OnTriggerEnter(Collider c)
	{
		if (c.gameObject.name.Equals("Player") && !firstConversation)
		{           
			first.Play(this.transform.position.x, this.transform.position.y + dialogueYOffSet);
			firstConversation = true;
			nextText++;

		}
	}
}



