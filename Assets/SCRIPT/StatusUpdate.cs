﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatusUpdate : MonoBehaviour {
	public Image physicalBar;
	public Image mentalBar;
	public Image faithBar;

	public static bool physicalHit = false;
	public static bool mentalHit = false;
	public static bool faithHit = false;


	public static float physicalStatus;
	public static float mentalStatus;
	public static float faithStatus;

    GameObject wakeUpFadeBlackScreen;

	float lossAmount = 0.002f;

	int hitTimer=0;

    public static bool startBool = false;
	// Use this for initialization
	void Start () {
        


	}

	// Update is called once per frame
	void FixedUpdate () {
        if(startBool)
        {
            wakeUpFadeBlackScreen = GameObject.Find("wake");
            physicalStatus = physicalBar.fillAmount;
            mentalStatus = mentalBar.fillAmount;
            faithStatus = faithBar.fillAmount;
            startBool = false;
        }
		if (physicalHit)
		{
			if (hitTimer < 100)
			{
				physicalBar.GetComponent<Animator>().SetBool("hit", true);
				physicalBar.fillAmount -= lossAmount;
				physicalStatus = physicalBar.fillAmount; 
				hitTimer++;
			}
			else
			{

				hitTimer = 0;
				physicalBar.GetComponent<Animator>().SetBool("hit", false);
				physicalHit = false;
                
                StartCoroutine(WaitAndLoad());
            }

		}
		else if (mentalHit)
		{
			if (hitTimer < 100)
			{
				mentalBar.GetComponent<Animator>().SetBool("hit", true);
				mentalBar.fillAmount -= lossAmount;
				mentalStatus = mentalBar.fillAmount;
				hitTimer++;
			}
			else
			{
				hitTimer = 0;
				mentalBar.GetComponent<Animator>().SetBool("hit", false);
				mentalHit = false;
                StartCoroutine(WaitAndLoad());
            }

		}
		else if (faithHit)
		{
			if (hitTimer < 100)
			{
				faithBar.GetComponent<Animator>().SetBool("hit", true);
				faithBar.fillAmount -= lossAmount;
				faithStatus = faithBar.fillAmount;
				hitTimer++;
			}
			else
			{
				hitTimer = 0;
				faithBar.GetComponent<Animator>().SetBool("hit", false);
				faithHit = false;
                StartCoroutine(WaitAndLoad());
            }

		}
	}
	public static void Hit(string damageType)
	{

		if(damageType=="p")
		{
			physicalHit = true;
		}
		else if (damageType == "m")
		{
			mentalHit = true;
		}
		else if (damageType == "f")
		{
			faithHit = true;
		}
		else
		{
			Debug.Log("Error: Please input correct damage type. You entered: "+damageType);
		}
	}
    IEnumerator WaitAndLoad()
    {
        wakeUpFadeBlackScreen.GetComponent<Animator>().SetBool("fade", true);
        yield return new WaitForSeconds(2.5f);
        SceneManagement.loadLevel();
    }


}
